
# Installation de GitLab

Il existe de nombreuses manières d'installer GitLab. Elles sont [documentées sur le site officiel](https://docs.gitlab.com/ee/install/install_methods.html).  

Dans le cadre de la formation nous allons mettre en pratique une installation avec Docker. Plus concrètement, nous allons démarrer un conteneur Docker avec un serveur gitlbab, puis un autre conteneur avec un "gitlab runner". Les gitlab runner seront utilisés dans la suite de la formation pour les pipelines de CI/CD.


## Prérequis

Docker doit être installé sur votre machine.

## Démarrer une instance de GitLab avec docker

Voici un exemple de commande pouvant être lancé (par la suite) pour un lancer un serveur gitlab (prend plusieurs minutes) : 

```bash
sudo docker run --detach \
  --hostname localhost \
  --publish 443:443 --publish 80:80 --publish 2222:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ce:latest
```

🗣 Discussion sur l'utilité de chaque ligne / option / argument:  

- ❓ Comment s'appelle le conteneur crée par la commande ci-dessus ?
- ❓ Est-ce la version communautaire ou la version entrerprise qui est lancée ?
- ❓ Que faudra-t-il tapper dans le navigateur web pour atteindre le serveur gitlab ?
- ❓ Le conteneur créé sera-t-il stoppé si après avoir lancé la commande on fait Ctrl-C (ou si on ferme le terminal)
- ❓ Une fois la commande lancée, le serveur gitlab sera-t-il disponible suite au redémarrage de la machine ?
- ❓ Si on supprime le conteneur gitlab, les données de configuration et les données utilisateurs seront-elles perdues ?
- ❓ A cet effet, à quoi doit-on veiller avant cette commande ?
- ❓ La mémoire partagée proposée par défaut par docker est-elle suffisante pour gitlab ?
- ❓ Si vous voulez accéder en ssh à la machine qui héberge le conteneur, cela va-t-il interférer avec l'accès ssh au conteneur gitlab ?


🏗 Tâches à effectuer :
- Veillez à définir la variable d'environnement `GITLAB_HOME` avec un `export` ou un fichier nommé `.env`.
- Lancez la commande docker ci-dessus 
- Testez l'accès à votre instance de gitlab depuis un navigateur (au bout de 2-3 mn environ)
- Dans un autre terminal récupérez le mot de passe initial
    ```bash
        sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
    ```
- Connectez-vous avec le login `root` et le mot de passe récupéré à l'étape précédente.


Si vous devez supprimer votre container Docker : 
```bash
sudo docker stop gitlab  # Stoppe le container gitlab
sudo docker rm gitlab    # Supprime le container gitlab
```

## Démarrer une instance de GitLab depuis fichier "docker compose"

🏗 Créez un fichier `docker-compose.ex1.yml` qui permet de lancer l'équivalent de la commande `docker run ...` avec `docker compose -f docker-compose.ex1.yml up`  


Ainsi commencera votre fichier : 

```yaml
### -----------------------------
### SERVICES
### -----------------------------
services:

  # -----------------------------
  # GitLab Server
  # -----------------------------
  gitlab:
    image: 'gitlab/gitlab-ce:latest'
```

Si vous voulez renseigner préalablement le mot de passe root de votre serveur gitlab, vous pouvez ajouter :

```yaml
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        gitlab_rails['initial_root_password'] = "${GITLAB_PASSWD}"
```

> :bulb: Cela implique bien sûr de renseigner la variable d'environnement `GITLAB_PASSWD`. On ne l'écrit pas "en dûr" pour des raisons évidentes de sécurité. Ajoutez-le dans un fichier nommé `.env` à côté de votre fichier docker-compose. Attention de ne jamais "add" un fichier `.env` avec GIT (gitignore ?) pour des raisons de sécurité évidentes. 
>    ```bash
>    GITLAB_HOME="./dk_gl_volume"  
>    GITLAB_PASSWD="UpToIntegrateur"  
>    ```


Faites valider votre fichier "docker compose" et son bon fonctionnement par le formateur avant de passer à la suite.


## Ajouter un Runner manuellement


En supposant que votre conteneur est opérationnel à partir de l'étape précédente, nous allons maintenant :
- Stopper le conteneur gitlab
- Ajouter un service `gitlab-runner` à notre fichier docker compose, afin de lancer un conteneur avec un "Gitlab Runner"
- Associer manuellement le "Gitlab Runner" à Gitlab

> :bulb: Un GitLab Runner est un outil qui automatise le processus de test et de déploiement de votre code   


🏗 Pour cela :
- Stoppez votre conteneur
- Copiez votre fichier `docker-compose.ex1.yml` dans un fichier `docker-compose.ex2.yml` et ajoutez-y le contenu suivant :

    ```yaml
  # -----------------------------
  # GitLab Runner
  # -----------------------------
  gitlab-runner:
    image: gitlab/gitlab-runner:latest
    container_name: 'gitlab-runner'
    restart: always
    depends_on:
      - gitlab    
    volumes:
      - './dk_gl_volume/gitlab-runner-config:/etc/gitlab-runner'
      - '/var/run/docker.sock:/var/run/docker.sock'
    ```
- Lancez le nouveau fichier docker-compose : `docker compose -f docker-compose.ex2.yml up`
- Dans l'interface `Admin`, allez dans `CI/CD` --> `Runners` et cliquez en haut à droite sur le bouton bleu `New instance runner`  

    ![](img/Runners.png)  

- Complétez ainsi la fenêtre qui apparait :  

    ![](img/NewInstanceRunner.png)

- Terminez en cliquant sur le boutou bleu "Create runner". Une page s'affiche, contenant un token. Copiez précieusement ce token.

- Dans un nouveau terminal, tapez la commande suivante pour ouvrir un termial dans le conteneur du runner gitlab : 
    ```bash
    docker exec -it gitlab-runner bash
    ```

- Une fois que vous êtes bien dans le terminal du conteneur, tapez la commande suivante en remplaçant le token avec les étoiles par le vôtre : 
    ```bash
    gitlab-runner register  --url http://gitlab  --token glrt-*******
    ```

> :bulb: l'URL `http://gitlab` est accessible par `gitlab-runner`, à cause du service `gitlab` défini dans le docker-compose, dont le service `gitlab-runner` dépend (`depends_on`) 


- Suite à cette commande, complétez le prompt interractif ainsi :

  - executor: `docker`
  - docker image: `python:alpine`

- Retournez sur [http://localhost/admin/runners](http://localhost/admin/runners). Le "Status" doit être passé à Online (en vert).


Faites valider par le formateur avant de passer à la suite.


## Ajouter un Runner automatiquement

Il est possible d'automatiser l'association du Runner que nous venons de faire manuellement. 

🏗 A cet effet, copiez votre fichier `docker-compose.ex2.yml` dans un fichier `docker-compose.ex3.yml` et suivez les instructions suivantes :
- Dans le service `gitlab`, en attribut de `environment`, ajoutez la ligne :
    ```yaml
        GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN: ${GITLAB_TOKEN}   
    ```
> :bulb: Cela vous permet de définir vous même un TOKEN, afin de renseigner le même dans le service `gitlab-runner`

- Toujours dans le service `gitlab`, ajoutez un attribut avec les lignes suivantes :  

    ```yaml
        healthcheck:
            test: curl --fail http://localhost/users/sign_in || exit 1
            interval: 1m
            timeout: 10s
            retries: 5
            start_period: 30s   
    ```

> :bulb: Ce `healthcheck` nous permet de tester si le service `gitlab` est opérationnel. Comme ce dernier met du temps à démarrer, il est important  de tester sa disponibilité avant d'automatiser des tâches.

- Dans le service `gitlab-runner`, modifiez les lignes après `depends_on` : 
    
    ```yaml
    depends_on:
      gitlab:
        condition: service_healthy  
    ```

- Dans le service `gitlab-runner`, ajoutez les lignes suivantes : 

    ```yaml
        entrypoint: [""]
        command: ["/bin/sh", "-c", "gitlab-runner register \
                                        --non-interactive \
                                        --url 'http://gitlab' \
                                        --docker-network-mode 'host' \
                                        --registration-token '${GITLAB_TOKEN}' \ 
                                        --executor 'docker' \
                                        --docker-image 'python:alpine' \
                                        --name 'mon-runner' \
            && gitlab-runner run --user=gitlab-runner --working-directory=/etc/gitlab-runner"]
    ``` 

> :bulb: Dans un fichier docker-compose, l'option entrypoint permet de spécifier une commande qui sera exécutée au démarrage du conteneur. En définissant `entrypoint: [""]`, vous réinitialisez l'entrypoint par défaut du conteneur, ce qui signifie que le conteneur démarrera sans exécuter la commande par défaut configurée dans l'image Docker (ici de l'image `gitlab/gitlab-runner:latest`). Cela vous permet de définir une nouvelle commande de démarrage via l'option command. Cela nous permets d'ajouter des options.

> :bulb: Vous constatez nottement le `GITLAB_TOKEN` qui apparait à nouveau, afin d'associer les 2 services.  


- Nous allons tester ce nouveau fichier. Afin de ne pas tirer de fausses conclusions, nous allons:
  - Arrêter nos conteneurs
  - Supprimer votre docker volume : `sudo rm -rf <le chemin vers votre docker volume>` afin de ne pas garder l'association au précédent Runner.
  - Ajouter `GITLAB_TOKEN` au fichier `.env`. Par exemple : `GITLAB_TOKEN=up2integrateur42`
  - Relancer avec le nouveau fichier : `docker compose -f docker-compose.ex3.yml up`          

```bash
docker compose -f docker-compose.auto.yml
```