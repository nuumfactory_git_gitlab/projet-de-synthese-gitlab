# Administration de GitLab


A partir du menu "Admin" de Gitlab : 

- Affichez un bandeau d'annonce pendant 1h disant "Maintenance en cour"

- Settings :
  - Paramétrez les groupes comme étant "internes" par défaut (accessibles aux personnes logguées)
  - Paramétrez les projets comme étant "privés" par défaut (accessibles aux personnes ayant reçu les droits)
  - Faites en sorte qu'un nouvel utilisateur soit obligé de répondre au mail de confirmation avant de pouvoir se logguer
  - Faites en sorte que le repo par défaut ne soit pas "main" mais "master"
  - Ajoutez le logo SNCF comme Flavicon (icône de l'onglet de navigation)
  - Ajoutez une barre header en "violet SNCF" avec écrit dedans "GITLAB de la SNCF"


- A partir de ce code que vous adapterez, d'un webhook que vous créerez, et éventuellement d'une adaptation du lancement de gitlab, logguez l'activité de l'utilisateur "jp.farandou" dans la sortie standard du terminal où le code est executé : 

    ```python
    from flask import Flask, request, jsonify

    app = Flask(__name__)

    @app.route('/', methods=['POST'])
    def webhook():
        data = request.json
        print("Received webhook data:")
        print(data)
        return jsonify(success=True), 200

    if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0', port=5000)
    ```

    A cet effet vous pouvez créer l'utilisateur mentionné et effectuer des tâches en son nom.